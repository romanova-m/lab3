package ru.mirea.data;

import ru.mirea.data.DataSourceAPI;
import java.util.Set;
import java.util.TreeSet;
import java.sql.*;


public class DataSource implements DataSourceAPI {
	
	private Connection conn = null;
	private Statement stmt = null;



	public DataSource() {
		
		getConnection();
		initDB();
		//closeDB();
	}

		

	private void closeDB() {

		try {
			if (conn != null) { conn.close(); conn = null;}
			if (stmt != null) { stmt.close(); stmt = null;}
		}
		catch(SQLException se) {
                        se.printStackTrace();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
	}



	private void getConnection() {

                try {
			Class.forName("org.h2.Driver");
			conn = DriverManager.getConnection("jdbc:h2:~/db/test","sa","");
			stmt = conn.createStatement();
		}
		catch(SQLException se) {
                        se.printStackTrace();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
	}



	private void bdUpt(String sql) {

		try {
			stmt.executeUpdate(sql);
		}
		catch(SQLException se) {
                        se.printStackTrace();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
	}



	private void initDB() {	

		bdUpt("DROP TABLE IF EXISTS WeatherForecast");
		bdUpt("CREATE TABLE WeatherForecast " + "(id INTEGER not NULL, " +
			" city VARCHAR(255), " + " weather VARCHAR(255), " +
			" date INTEGER, " + " PRIMARY KEY ( id ))");
		bdUpt("INSERT INTO WeatherForecast " + "VALUES (1, 'Omsk', '+1', 18)");
               	bdUpt("INSERT INTO WeatherForecast " + "VALUES (2, 'Moscow', '+15', 18)");
               	bdUpt("INSERT INTO WeatherForecast " + "VALUES (3, 'Perm', '+5', 18)");
		bdUpt("INSERT INTO WeatherForecast " + "VALUES (4, 'Sochi', '+25', 18)");
		bdUpt("INSERT INTO WeatherForecast " + "VALUES (5, 'Vologda', '+5', 18)");
      	}



	public Set <String> cities () {

		Set<String> cities = new TreeSet<String>();
		try {
			getConnection();
               		ResultSet rs = stmt.executeQuery("SELECT city FROM WeatherForecast");
			while(rs.next()) {        
				String city = rs.getString("city");
				cities.add(city);
			}
			rs.close();
			//closeDB();
        	}
        	catch(SQLException se) {
                	se.printStackTrace();
        	}
        	catch(Exception e) {
                	e.printStackTrace();
        	}
        	finally {
           		//closeDB();
        	}
		return cities;
	}




	public String getByCity (String city) {	

		String result = null;
		try {
			getConnection();
			ResultSet rs = stmt.executeQuery("SELECT weather FROM " +
				"WeatherForecast WHERE city = '" + city + "'");
			while(rs.next()){
				result = rs.getString("weather");
			}
			rs.close();
			//closeDB();
		}
		catch(SQLException se) {
			se.printStackTrace();
        	}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt!=null) stmt.close();
			}
			catch(SQLException se2) {}
                	try {
				if(conn!=null) conn.close();
			}
			catch(SQLException se){se.printStackTrace();}
		}
		return result;
	}
}
