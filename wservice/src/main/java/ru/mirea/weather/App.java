package ru.mirea.weather;

import ru.mirea.data.DataSource;

public class App {
public static void main( String[] args ) {
                
		DataSource dc = new DataSource();
                CustomQueue inQueue = new CustomQueue();
                CustomQueue outQueue = new CustomQueue();

                TaskGenerator tg = new TaskGenerator(inQueue, dc);
                TaskExecutor te = new TaskExecutor(inQueue, outQueue, dc);
                Logger lg = new Logger(outQueue);

                Thread ttg = new Thread(tg);
                Thread tlg = new Thread(lg);
                Thread t1 = new Thread(te);
                //Thread t2 = new Thread(te);
		//Thread t3 = new Thread(te);
                //Thread t4 = new Thread(te);


                ttg.start();
                tlg.start();
                t1.start();
                //t2.start();
		//t3.start();
		//t4.start();

                try {
                        Thread.sleep(300);
                } catch (InterruptedException e) {
                        e.printStackTrace();
                }

                ttg.interrupt();
                t1.interrupt();
                //t2.interrupt();
		//t3.interrupt();
		//t4.interrupt();
                tlg.interrupt();
}
}
