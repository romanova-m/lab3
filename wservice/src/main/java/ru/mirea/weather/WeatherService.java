package ru.mirea.weather;

import ru.mirea.data.DataSource;
import java.util.Set;
import java.lang.String;

public class WeatherService {

	private DataSource datasource;

	

	public WeatherService () {
		this.datasource = new DataSource();
	} 
	
	public Set<String> availableCities () {
		System.out.println("Available cities count " + this.datasource.cities().size());
		return this.datasource.cities();
	}

	public String getWeather (String city) {
		System.out.println("City " + city + " requested");
		return this.datasource.getByCity(city);
	}
}
